package test.java.ru.crystals;

import main.java.ru.crystals.price.InvalidPriceCollectionException;
import main.java.ru.crystals.price.Price;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static main.java.ru.crystals.price.PriceUtils.updatePrices;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class PriceUtilsTest {

    @Test
    public void testUpdatePrices() {
        List<Price> oldPrices = new ArrayList<>();
        oldPrices.add(new Price("122856", 1, 1, new Date(2013, 0, 1, 0, 0, 0), new Date(2013, 0, 31, 23, 59, 59), 11000));
        oldPrices.add(new Price("122856", 2, 1, new Date(2013, 0, 10, 0, 0, 0), new Date(2013, 0, 20, 23, 59, 59), 99000));
        oldPrices.add(new Price("6654", 1, 2, new Date(2013, 0, 1, 0, 0, 0), new Date(2013, 0, 31, 0, 0, 0), 5000));

        List<Price> newPrices = new ArrayList<>();
        newPrices.add(new Price("122856", 1, 1, new Date(2013, 0, 20, 0, 0, 0), new Date(2013, 1, 20, 23, 59, 59), 11000));
        newPrices.add(new Price("122856", 2, 1, new Date(2013, 0, 15, 0, 0, 0), new Date(2013, 0, 25, 23, 59, 59), 92000));
        newPrices.add(new Price("6654", 1, 2, new Date(2013, 0, 12, 0, 0, 0), new Date(2013, 0, 13, 0, 0, 0), 4000));

        List<Price> updatedPrices = new ArrayList<>();
        updatedPrices.add(new Price("122856", 1, 1, new Date(2013, 0, 1, 0, 0, 0), new Date(2013, 1, 20, 23, 59, 59), 11000));
        updatedPrices.add(new Price("122856", 2, 1, new Date(2013, 0, 15, 0, 0, 0), new Date(2013, 0, 25, 23, 59, 59), 92000));
        updatedPrices.add(new Price("122856", 2, 1, new Date(2013, 0, 10, 0, 0, 0), new Date(2013, 0, 15, 0, 0, 0), 99000));
        updatedPrices.add((new Price("6654", 1, 2, new Date(2013, 0, 12, 0, 0, 0), new Date(2013, 0, 13, 0, 0, 0), 4000)));
        updatedPrices.add((new Price("6654", 1, 2, new Date(2013, 0, 13, 0, 0, 0), new Date(2013, 0, 31, 0, 0, 0), 5000)));
        updatedPrices.add((new Price("6654", 1, 2, new Date(2013, 0, 1, 0, 0, 0), new Date(2013, 0, 12, 0, 0, 0), 5000)));

        List<Price> result = null;
        try {
            result = (List<Price>) updatePrices(oldPrices, newPrices);
        } catch (InvalidPriceCollectionException e) {
            e.printStackTrace();
        }
        assertEquals("testUpdatePrices subTest1", updatedPrices, result);

        try {
            updatePrices(oldPrices, null);
        } catch (Exception e) {
            assertTrue("testUpdatePrices subTest2", e instanceof InvalidPriceCollectionException);
            assertEquals("testUpdatePrices subTest3", "Invalid collection with new prices", e.getMessage());
        }

        try {
            updatePrices(new ArrayList<Price>(), newPrices);
        } catch (Exception e) {
            assertTrue("testUpdatePrices subTest4", e instanceof InvalidPriceCollectionException);
            assertEquals("testUpdatePrices subTest5", "Invalid collection with old prices", e.getMessage());
        }
    }
}