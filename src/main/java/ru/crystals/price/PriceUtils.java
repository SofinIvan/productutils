package main.java.ru.crystals.price;

import java.util.*;

public class PriceUtils {

    public static Collection<Price> updatePrices(Collection<Price> oldPrices, Collection<Price> newPrices) throws InvalidPriceCollectionException {
        validatePriceCollections(oldPrices, newPrices);
        Map<String, List<Price>> oldPricesTempMap = convertPriceCollectionToMap(oldPrices);
        Map<String, List<Price>> newPricesTempMap = convertPriceCollectionToMap(newPrices);

        List<Price> result = new ArrayList<>();
        for (Map.Entry<String, List<Price>> entry : newPricesTempMap.entrySet()) {
            if (!oldPricesTempMap.containsKey(entry.getKey())) {
                result.addAll(entry.getValue());
            } else {
                List<Price> oldProductPrices = oldPricesTempMap.get(entry.getKey());
                List<Price> newProductPrices = newPricesTempMap.get(entry.getKey());
                updateProductPrices(oldProductPrices, newProductPrices, result);
            }
        }
        return result;
    }

    private static void updateProductPrices(List<Price> oldProductPrices, List<Price> newProductPrices, List<Price> result) {
        for (Price oldPrice : oldProductPrices) {
            for (Price newPrice : newProductPrices) {
                if (oldPrice.getDepart() == newPrice.getDepart() && oldPrice.getNumber() == newPrice.getNumber()) {
                    long oldPriceEndTime = oldPrice.getEnd().getTime();
                    long newPriceBeginTime = newPrice.getBegin().getTime();
                    long newPriceEndTime = newPrice.getEnd().getTime();

                    if (newPriceBeginTime < oldPriceEndTime) {
                        if (oldPrice.getValue() == newPrice.getValue()) {
                            oldPrice.setEnd(newPrice.getEnd());
                            result.add(oldPrice);
                        } else {
                            result.add(newPrice);
                            if (oldPriceEndTime > newPriceEndTime) {
                                result.add(new Price(oldPrice.getProduct_code(),
                                        oldPrice.getNumber(),
                                        oldPrice.getDepart(),
                                        newPrice.getEnd(),
                                        oldPrice.getEnd(),
                                        oldPrice.getValue()));
                            }
                            oldPrice.setEnd(newPrice.getBegin());
                            result.add(oldPrice);
                        }
                    } else {
                        result.add(oldPrice);
                        result.add(newPrice);
                    }
                }
            }
        }
    }

    private static void validatePriceCollections(Collection<Price> oldPrices, Collection<Price> newPrices) throws InvalidPriceCollectionException {
        if (oldPrices == null || oldPrices.isEmpty()) {
            throw new InvalidPriceCollectionException("Invalid collection with old prices");
        }
        if (newPrices == null || newPrices.isEmpty()) {
            throw new InvalidPriceCollectionException("Invalid collection with new prices");
        }
    }

    private static Map<String, List<Price>> convertPriceCollectionToMap(Collection<Price> prices) {
        Map<String, List<Price>> map = new HashMap<>();
        for (Price price : prices) {
            if (price != null) {
                String code = price.getProduct_code();
                if (!map.containsKey(code)) {
                    List<Price> oneProductPrices = new ArrayList<>();
                    oneProductPrices.add(price);
                    map.put(code, oneProductPrices);
                } else {
                    map.get(code).add(price);
                }
            }
        }
        return map;
    }
}