package main.java.ru.crystals.price;

public class InvalidPriceCollectionException extends Exception {
    public InvalidPriceCollectionException(String message) {
        super(message);
    }
}